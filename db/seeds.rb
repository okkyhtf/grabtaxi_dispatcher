# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Driver.delete_all
Driver.create!(name: 'Adam Ashton', phone: '+62811234567', latitude: (rand -90.000000..90.000000).round(6), longitude: (rand -180.000000..180.000000).round(6))
Driver.create!(name: 'Brian Borrowick', phone: '+62812234644', latitude: (rand -90.000000..90.000000).round(6), longitude: (rand -180.000000..180.000000).round(6))
Driver.create!(name: 'Charlie Chuckles', phone: '+62813234698', latitude: (rand -90.000000..90.000000).round(6), longitude: (rand -180.000000..180.000000).round(6))
Driver.create!(name: 'Damian Delaware', phone: '+62814234526', latitude: (rand -90.000000..90.000000).round(6), longitude: (rand -180.000000..180.000000).round(6))
Driver.create!(name: 'Edward Ernst', phone: '+62815349347', latitude: (rand -90.000000..90.000000).round(6), longitude: (rand -180.000000..180.000000).round(6))
Driver.create!(name: 'Finn Flannigan', phone: '+62816287667', latitude: (rand -90.000000..90.000000).round(6), longitude: (rand -180.000000..180.000000).round(6))
Driver.create!(name: 'Gregory Georgetown', phone: '+62817255667', latitude: (rand -90.000000..90.000000).round(6), longitude: (rand -180.000000..180.000000).round(6))
Driver.create!(name: 'Hendrick Henderson', phone: '+62818200967', latitude: (rand -90.000000..90.000000).round(6), longitude: (rand -180.000000..180.000000).round(6))
Driver.create!(name: 'Ian Impala', phone: '+62819235467', latitude: (rand -90.000000..90.000000).round(6), longitude: (rand -180.000000..180.000000).round(6))
Driver.create!(name: 'Jeremy Jefferson', phone: '+62810234887', latitude: (rand -90.000000..90.000000).round(6), longitude: (rand -180.000000..180.000000).round(6))
