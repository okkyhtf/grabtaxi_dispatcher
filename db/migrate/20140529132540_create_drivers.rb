class CreateDrivers < ActiveRecord::Migration
  def change
    create_table :drivers do |t|
      t.string :name
      t.string :phone
      t.decimal{10,6} :latitude
      t.decimal{10,6} :longitude

      t.timestamps
    end
  end
end
