json.array!(@drivers) do |driver|
  json.extract! driver, :id, :name, :phone, :latitude, :longitude
  json.url driver_url(driver, format: :json)
end
