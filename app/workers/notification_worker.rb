class NotificationWorker
  include Sidekiq::Worker
  
  sidekiq_options queue: "dispatcher"

  def perform(name, phone, booking_id)
    message =  "You have been assigned to pick up somebody for Booking ID #{booking_id}!"
    puts "\n\nNotificationWorker: Sent SMS to #{name} at #{phone} with message \"#{message}\""
  end
end
