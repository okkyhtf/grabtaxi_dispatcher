class Driver < ActiveRecord::Base
  validates :name, :phone, presence: true
  validates :phone, uniqueness: true
  
  acts_as_mappable default_units: :kms, default_formula: :sphere, distance_field_name: :distance, lat_column_name: :latitude, lng_column_name: :longitude
  
  def self.get_closest_to(latitude, longitude)
    return Driver.closest(:origin => [latitude, longitude]).first
  end
end
