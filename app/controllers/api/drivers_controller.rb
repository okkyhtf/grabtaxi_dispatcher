class Api::DriversController < ApplicationController
  require 'rest_client'
  
  http_basic_authenticate_with name: "nostra", password: "welcome1"
  before_action :fetch_driver, except: [:index, :create, :assign, :closest_to]
  skip_before_action :verify_authenticity_token
  respond_to :json
  
  BOOKINGS_REST_API_BASE_URL = 'http://localhost:3000/api'
  BOOKINGS_REST_API_USERNAME = 'nostra'
  BOOKINGS_REST_API_PASSWORD = 'welcome1'
  
  def fetch_driver
    @driver = Driver.find(params[:id])
  end
  
  def index
    @drivers = Driver.all
    respond_to do |format|
      format.json {render json: @drivers}
    end
  end

  def show
    respond_to do |format|
      format.json {render json: @driver}
    end
  end

  def create
    @driver = Driver.new(driver_params)
    respond_to do |format|
      if @driver.save
        format.json {render json: @driver, status: :created}
      else
        format.json {render json: @driver.errors, status: :unprocessable_entity}
      end
    end
  end

  def update
    respond_to do |format|
      if @driver.update_attributes(driver_params)
        format.json {head :no_content, status: :ok}
      else
        format.json {render json: @driver.errors, status: :unprocessable_entity}
      end
    end
  end

  def destroy
    respond_to do |format|
      if @driver.destroy
        format.json {head :no_content, status: :ok}
      else
        format.json {render json: @driver.errors, status: :unprocessable_entity}
      end
    end
  end
  
  def assign
    booking = get_booking_detail(params[:booking_id])
    driver = Driver.get_closest_to(booking[:pickup_latitude], booking[:pickup_longitude])
    respond_with update_driver_in_booking(params[:booking_id], driver.name, driver.phone)
  end
  
  private
  
  def get_booking_detail(booking_id)
    uri = "#{BOOKINGS_REST_API_BASE_URL}/bookings/#{booking_id}"
    rest_resource = RestClient::Resource.new(uri, BOOKINGS_REST_API_USERNAME, BOOKINGS_REST_API_PASSWORD)
    begin
      booking = rest_resource.get
    rescue Exception => e
      booking = {status: "Failed!"}
    end
    return ActiveSupport::JSON.decode(booking)
  end
  
  def update_driver_in_booking(booking_id, name, phone)
    uri = "#{BOOKINGS_REST_API_BASE_URL}/bookings/#{booking_id}"
    rest_resource = RestClient::Resource.new(uri, BOOKINGS_REST_API_USERNAME, BOOKINGS_REST_API_PASSWORD)
    payload = {booking: {driver_name: name, driver_phone: phone}}
    begin
      rest_resource.put payload
      status = {status: "Success!"}
      NotificationWorker.perform_async(name, phone, booking_id)
    rescue Exception => e
      status = {status: "Failed!"}
    end
    return status
  end
  
  def driver_params
    params.require(:driver).permit(:name, :phone, :latitude, :longitude)
  end
end
